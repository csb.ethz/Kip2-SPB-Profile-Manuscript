function [fitresult, gof] = powerModelFit(quantilesAllLow, quantilesAllHigh, doPlot)
%CREATEFIT(QUANTILESALLLOW,QUANTILESALLHIGH)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      X Input : quantilesAllLow
%      Y Output: quantilesAllHigh
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.

%  Auto-generated by MATLAB on 21-Nov-2018 14:07:20


%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( quantilesAllLow, quantilesAllHigh );

% Set up fittype and options.
% ft = fittype( 'poly1' );
% opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
% opts.Display = 'Off';
% opts.StartPoint = [0.0709280171698654 1.3004811420376];

ft = fittype( 'poly1' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
%opts.Robust = 'Bisquare';

% ft = fittype( 'smoothingspline' );
% opts = fitoptions( 'Method', 'SmoothingSpline' );
% opts.Normalize = 'on';
% opts.SmoothingParam = 0.999;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

if doPlot 
    % Create a figure for the plots.
    figure( 'Name', 'untitled fit 1' );

    % Plot fit with data.
    subplot( 2, 1, 1 );
    h = plot( fitresult, xData, yData );
    legend( h, 'quantilesAllHigh vs. quantilesAllLow', 'untitled fit 1', 'Location', 'NorthEast' );
    % Label axes
    xlabel quantilesAllLow
    ylabel quantilesAllHigh
    grid on

    % Plot residuals.
    subplot( 2, 1, 2 );
    h = plot( fitresult, xData, yData, 'residuals' );
    legend( h, 'untitled fit 1 - residuals', 'Zero Line', 'Location', 'NorthEast' );
    % Label axes
    xlabel quantilesAllLow
    ylabel quantilesAllHigh
    grid on
    
end

